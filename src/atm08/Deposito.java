/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm08;

import javax.swing.JOptionPane;

/**
 *
 * @author Daniel Diaz
 */
public class Deposito extends Transaccion {
    public Deposito(){
        lb_titulo.setText("Deposito");
        lb_titulo.setBounds(100, 20, 300, 40);
        lb_cuenta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_cuenta.setText("123");
    }
    
    protected void ejecutar(){
        JOptionPane.showMessageDialog(null, "Deposito", "InfoBox: " + "titleBar", JOptionPane.INFORMATION_MESSAGE);
    }
}
