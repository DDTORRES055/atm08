/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm08;

import javax.swing.JOptionPane;

/**
 *
 * @author Daniel Diaz
 */
public class Retiro extends Transaccion {
    public Retiro(){
        lb_titulo.setText("Retiro");
        lb_titulo.setBounds(100, 20, 300, 40);
        lb_cuenta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_cuenta.setText("123");
    }
    
    public void ejecutar(){
        JOptionPane.showMessageDialog(null, "Retiro", "InfoBox: " + "titleBar", JOptionPane.INFORMATION_MESSAGE);
    }
}
