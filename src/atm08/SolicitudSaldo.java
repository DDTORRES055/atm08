/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm08;

import javax.swing.JOptionPane;

/**
 *
 * @author Daniel Diaz
 */
public class SolicitudSaldo extends Transaccion {
    
    private javax.swing.JLabel lb_saldototal;
    private javax.swing.JTextField txt_saldototal;
    private javax.swing.JLabel lb_saldodisponible;
    private javax.swing.JTextField txt_saldodisponible;
    
    public SolicitudSaldo(){
        lb_titulo.setText("Consultar saldo");
        lb_titulo.setBounds(100, 20, 300, 40);
        lb_cuenta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_cuenta.setText("123");
        lb_monto.setVisible(false);
        btn_100.setVisible(false);
        btn_200.setVisible(false);
        btn_300.setVisible(false);
        btn_500.setVisible(false);
        btn_confirmar.setVisible(false);
        btn_cancelar.setVisible(false);
        btn_otracantidad.setVisible(false);
        lb_op1.setVisible(false);
        lb_op2.setVisible(false);
        lb_op3.setVisible(false);
        lb_op4.setVisible(false);
        lb_op5.setVisible(false);
        lb_op6.setVisible(false);
        lb_$.setVisible(false);
        lb_borde.setVisible(false);
        txt_monto.setVisible(false);
        
        lb_saldototal = new javax.swing.JLabel();
        txt_saldototal = new javax.swing.JTextField();
        lb_saldodisponible = new javax.swing.JLabel();
        txt_saldodisponible = new javax.swing.JTextField();
        
        
        lb_saldototal.setBackground(new java.awt.Color(255, 255, 255));
        lb_saldototal.setFont(new java.awt.Font("Tahoma", 1, 18)); 
        lb_saldototal.setForeground(new java.awt.Color(255, 255, 255));
        lb_saldototal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb_saldototal.setText("Saldo total:  $");
        Transaccion.add(lb_saldototal);
        lb_saldototal.setBounds(80, 300, 180, 30);
        
        txt_saldototal.setBackground(new java.awt.Color(204, 204, 204));
        txt_saldototal.setFont(new java.awt.Font("Tahoma", 1, 12)); 
        Transaccion.add(txt_saldototal);
        txt_saldototal.setBounds(260, 300, 140, 30);
        txt_saldototal.setEnabled(false);
        
        lb_saldodisponible.setBackground(new java.awt.Color(255, 255, 255));
        lb_saldodisponible.setFont(new java.awt.Font("Tahoma", 1, 18)); 
        lb_saldodisponible.setForeground(new java.awt.Color(255, 255, 255));
        lb_saldototal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb_saldodisponible.setText("Saldo disponible:  $");
        Transaccion.add(lb_saldodisponible);
        lb_saldodisponible.setBounds(105, 400, 180, 30);
        
        txt_saldodisponible.setBackground(new java.awt.Color(204, 204, 204));
        txt_saldodisponible.setFont(new java.awt.Font("Tahoma", 1, 12));
        Transaccion.add(txt_saldodisponible);
        txt_saldodisponible.setBounds(280, 400, 140, 30);
        txt_saldodisponible.setEnabled(false);
        
        ejecutar();
    }
    
    protected void ejecutar(){
        JOptionPane.showMessageDialog(null, "Solicitud saldo", "InfoBox: " + "titleBar", JOptionPane.INFORMATION_MESSAGE);
    }
}
